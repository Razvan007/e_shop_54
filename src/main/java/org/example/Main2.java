package org.example;

import org.example.config.DatabaseConfig;
import org.example.entity.Rating;
import org.example.entity.UserFeedback;
import org.example.repository.UserFeedbackRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class Main2 {
    public static UserFeedbackRepository userFeedbackRepo = new UserFeedbackRepository();
    public static void main(String[] args) {   // prima data intra in metoda main din clasa in care am dat Run
        UserFeedback uf1 = new UserFeedback(1, Rating.GOOD, "Super florarie");
        UserFeedback uf2 = new UserFeedback(2, Rating.GOOD, "Super florarie"); //Se creaza un obiect de tip UserFeedback
                                                                                                    // (exista doar in memoria aplicatiri Java. Nu exista inca in baza de date)
        userFeedbackRepo.createUserFeedback(uf1);
        userFeedbackRepo.createUserFeedback(uf2);
        System.out.println(userFeedbackRepo.getAllUserFeedback());

        // Update
        uf1.setFeedbackDescription("Ne-a placut foarte mult floraria");
        userFeedbackRepo.updateUserFeedback(uf1);
        System.out.println(userFeedbackRepo.getAllUserFeedback());

        // delete
        userFeedbackRepo.deleteUserFeedback(uf2);
        System.out.println(userFeedbackRepo.getAllUserFeedback());
    }
}
