package org.example.repository;

import org.example.config.DatabaseConfig;
import org.example.entity.UserFeedback;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;

public class UserFeedbackRepository {
    private final SessionFactory sessionFactory = DatabaseConfig.getSessionFactory();
    public UserFeedbackRepository() {
    }
    public void createUserFeedback (UserFeedback uf) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.persist(uf);
        transaction.commit();
        session.close();
    }

    public List<UserFeedback> getAllUserFeedback() {
        List<UserFeedback> userFeedbacks = new ArrayList<>();
        Session session = sessionFactory.openSession();
        userFeedbacks = session.createQuery("SELECT u FROM UserFeedback u", UserFeedback.class).getResultList();
        session.close();
        return userFeedbacks;
    }

    public void updateUserFeedback(UserFeedback uf) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.merge(uf);
        transaction.commit();
        session.close();
    }

    public void deleteUserFeedback(UserFeedback uf) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.remove(uf);
        transaction.commit();
        session.close();
    }
}
