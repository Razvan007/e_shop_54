package org.example.repository;

import org.example.entity.Client;
import org.example.entity.Services;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;

public class ServicesRepository {
    private final SessionFactory sessionFactory;

    public ServicesRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void save(Client client) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.persist(client);
        transaction.commit();
        session.close();
    }

    public List<Services> getAll() {
        List<Services> services = new ArrayList<>();
        Session session = sessionFactory.openSession();
        // sql: SELECT * FROM client;
        // acesta este un query de hibernate (e usor diferit de Query-ul din SQL)
        // hql: SELECT s FROM Services s
        // selecteaza toate randurile din tabela Client
        // "s" este doar un alias pentru un rand
        services = session.createQuery("SELECT s FROM Services s", Services.class).getResultList();
        session.close();
        return services;
    }
}