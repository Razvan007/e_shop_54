package org.example.repository;

import org.example.entity.Supplier;
import org.example.entity.UserFeedback;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;

public class SupplierRepository {
    private final SessionFactory sessionFactory;
    public SupplierRepository(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }

    // salveaza un supplier in baza de date
    public void save(Supplier supplier) {
        Session session = sessionFactory.openSession(); // deschide o sesiune de comunicare cu baza de date
        Transaction transaction = session.beginTransaction(); //incepe o tranzactie cu baza de date
        session.persist(supplier); // avem o singura modificare de facut, si anume: salvarea supplier-ului
        transaction.commit(); // salveaza in baza de date modificarile facute dupa deschiderea tranzactiei
        session.close(); // inchide sesiunea de comunicare cu baza de date
    }
    public List<Supplier> getAll() {
        List<Supplier> suppliers = new ArrayList<>();
        Session session = sessionFactory.openSession();
        suppliers= session.createQuery("SELECT s FROM Supplier s", Supplier.class).getResultList();
        session.close();
        return suppliers;
    }
}

