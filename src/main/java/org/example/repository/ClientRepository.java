package org.example.repository;

import org.example.entity.Client;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;

public class ClientRepository {
    private final SessionFactory sessionFactory;
    public ClientRepository(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }

    public void save(Client client) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.persist(client);
        transaction.commit();
        session.close();
    }

    public List<Client> getAll() {
        List<Client> clients = new ArrayList<>();
        Session session = sessionFactory.openSession();
        // sql: SELECT * FROM client;
        // acesta este un query de hibernate (e usor diferit de Query-ul din SQL)
        // hql: SELECT c FROM Client c
        // selecteaza toate randurile din tabela Client
        // "c" este doar un alias pentru un rand
        clients = session.createQuery("SELECT c FROM Client c", Client.class).getResultList();
        session.close();
        return clients;
    }
}
