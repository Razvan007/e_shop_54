package org.example.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

    @NoArgsConstructor
    @AllArgsConstructor
    @Getter
    @Setter
    @Entity

    public class Services {
        @Id
        private Integer id;
        @Column(name = "services type")
        private String name;
        @Column(name = "price")
        private Double price;
        @Column(name = "details")
        private Double details;
}
