package org.example.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Generated;
import lombok.NoArgsConstructor;
import lombok.Setter;

/*id
 name
 location*/
@NoArgsConstructor
@AllArgsConstructor
@Generated
@Setter
@Entity

public class Supplier {
    @Id
    private Integer id;
    private String name;
    private String location;
}
