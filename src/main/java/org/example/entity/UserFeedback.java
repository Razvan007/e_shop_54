package org.example.entity;

import jakarta.persistence.*;
import jdk.jfr.Enabled;
import lombok.*;

@Entity
@Table(name = "user_feedback") // atentie!!! @Table este pentru redenumirea tabelului; Iar @Entity este pentru crearea tabelului
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor

    public class UserFeedback {
        @Id // seteaza primari key-ul
        private Integer id;

        @Enumerated(value = EnumType.STRING) // se asigura ca in baza de date se salveaza cuvantul GOOD sau BAD si NU cifrele 1 sau 2
        private Rating rating;

        @Column(name = "feedback_description") // atentie @Column NU creaza coloana in sine; In schimb @Entity se ocupa de crearea tabelului cu coloanele lui
        private String feedbackDescription;    // @Column doar redenumeste coloana din feedbackDescription in feedback_description

    //User feedback1 este GOOD: Super florarie
    public String toString() {
        String s = "User Feedback " + id + " este " + rating + ": " + feedbackDescription;
        return s;
    }
}

